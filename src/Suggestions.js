import React, { Component } from "react";
import { connect } from "react-redux";
import { autocomplete } from "./actions";
import { getSuggestions } from "./selectors";


const colors = {
  func_name: "green",
  arg: "blue",
  table_col: "purple"
};

const HOC = connect(getSuggestions, {
  autocomplete
});

function Suggestions(props) {
  const { context, tokenRange: range, items, autocomplete } = props;

  if (context === "none" || !range) {
    return null;
  }

  return (
    <div
      style={{
        position: "absolute",
        zIndex: 10,
        top: "100%",
        width: 256,
        border: "2px solid #000",
        borderColor: colors[context],
        textAlign: "left",
        fontSize: 14,
        padding: 7
      }}
    >
      {items.map(item => {
        function doAutocomplete(e) {
          e.preventDefault();
          e.stopPropagation();
          autocomplete(range, item);
        }
        return (
          <p key={item} onClick={doAutocomplete} className="autocmpl-item">
            {item}
          </p>
        );
      })}
    </div>
  );
}

export default HOC(Suggestions);
