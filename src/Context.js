import React from "react";
import { connect } from "react-redux";
import { getContext } from "./selectors";

const HOC = connect(state => {
  return {
    contexts: getContext(state)
  };
});

function ContextEntry(props) {
  return (
    <pre className="ctx-entry">
      <code>{JSON.stringify(props.ctx, null, 2)}</code>
    </pre>
  );
}

function Context({ contexts }) {
  if (!contexts.length) {
    contexts = [{ context: "n/a" }];
  }
  return (
    <div>
      <h3>Parsing Context</h3>
      <div className="ctx">
        {contexts.map((ctx, i) => <ContextEntry key={i} ctx={ctx} />)}
      </div>
    </div>
  );
}

export default HOC(Context);
