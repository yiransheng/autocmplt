import React, { Component } from "react";
import { connect } from "react-redux";

import TestCases from './TestCases';
import Input from "./Input";
import CompletionContext from './CompletionContext';
import Suggestions from "./Suggestions";
import Tokens from "./Token";
import Context from './Context';

import { edit } from "./actions";

const HOC = connect(state => state, {
  edit
});

class App extends Component {
  render() {
    const { value, cursorAt, lastChangedBy, edit } = this.props;
    return (
      <div className="container">
        <div className="app">
          <h2>Demo</h2>
          <TestCases />
          <p>Only support click menu item to autocomplete.</p>
          <div style={{ height: 40, position: "relative" }}>
            <Input
              value={value}
              cursorAt={cursorAt}
              onChange={edit}
              autoFocus={lastChangedBy === "auto"}
            />
            <Suggestions />
          </div>
        </div>
        <div className="internal">
          <CompletionContext />
          <div className="divider" />
          <Tokens />
          <div className="divider" />
          <Context />
        </div>
      </div>
    );
  }
}

export default HOC(App);
