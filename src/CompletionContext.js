import React from "react";
import { connect } from "react-redux";
import { getSuggestions } from "./selectors";

const HOC = connect(getSuggestions);

function CompletionContext({ context }) {
  let ctx;
  switch (context) {
    case "func_name":
      ctx = "Function Name";
      break;
    case "table_col":
      ctx = "Dataframe Table Column";
      break;
    case "arg":
      ctx = "Function Argument";
      break;
    default:
      ctx = "None";
  }

  return (
    <h3>
      Autocompletion Context: <code className="autocmpl-ctx">{ctx}</code>
    </h3>
  );
}

export default HOC(CompletionContext);
