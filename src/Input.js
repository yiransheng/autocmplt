import React, { Component } from "react";

export default class Input extends Component {
  componentDidUpdate(prevProps) {
    if (prevProps.cursorAt !== this.props.cursorAt) {
      this.text.selectionStart = this.props.cursorAt;
      this.text.selectionEnd = this.props.cursorAt;
    }
    if (this.props.autoFocus && this.text) {
      this.text.focus();
    }
  }

  handleChange = () => {
    if (!this.text) {
      return;
    }
    const nextValue = this.text.value;
    const nextCursor = this.text.selectionEnd;

    if (nextValue !== this.props.value || nextCursor !== this.props.cursorAt) {
      this.props.onChange({
        value: nextValue,
        cursorAt: nextCursor
      });
    }
  };

  render() {
    const { value, cursorAt } = this.props;

    return (
      <input
        style={{ fontSize: 16, width: 256, padding: 7 }}
        ref={el => {
          this.text = el;
        }}
        value={value}
        onChange={this.handleChange}
        onKeyUp={this.handleChange}
        onMouseUp={this.handleChange}
      />
    );
  }
}
