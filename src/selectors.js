import { trySuggest } from "./engine";
import { createSelector, defaultMemoize } from "reselect";
import shallowEqual from "shallowequal";

const getSuggestionStates = defaultMemoize(trySuggest, shallowEqual);

export const getTokens = createSelector(
  getSuggestionStates,
  states => states.tokens
);

export const getSuggestions = createSelector(
  getSuggestionStates,
  states => states.suggestions
);

export const getContext = createSelector(getSuggestionStates, states => {
  const { result } = states;
  if (result.type === "ok") {
    return [];
  } else {
    return result.error.ctx;
  }
});
