import React from "react";
import { connect } from "react-redux";
import { autocomplete } from "./actions";

const HOC = connect(state => state, {
  autocomplete
});

const cases = [
  "=Table1!",
  "=DATAFRAME(",
  "=DIF",
  "=SUM(123, MAX(SO",
  "=SUM(AA + BB/",
];

function TestCases({ value, autocomplete }) {
  return (
    <div className="tests">
      <h4>Test cases, click to apply.</h4>
      {cases.map((c, i) => {
        return (
          <div
            key={i}
            className="test-case"
            onClick={() => {
              autocomplete([0, value.length], c);
            }}
          >
            {c}
          </div>
        );
      })}
    </div>
  );
}

export default HOC(TestCases);
