const defaultState = {
  value: "",
  cursorAt: 0,
  lastChangedBy: null,
};

export default function(state = defaultState, action) {
  switch (action.type) {
    case "EDITED": {
      return { ...action.payload, lastChangedBy: 'typing' };
    }
    case "AUTOCOMPLETE": {
      const { value } = state;
      const { range, replacement } = action.payload;
      const [start, end] = range;
      const before = value.slice(0, start);
      const after = value.slice(end);
      const cursorAt = before.length + replacement.length;

      return {
        value: before + replacement + after,
        cursorAt,
        lastChangedBy: 'auto',
      };
    }
    default:
      return state;
  }
}
