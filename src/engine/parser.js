/// Grammar
///

/// expr         ::= operation
/// operation    ::= unary ( "+" | "-" | "*" | "/" unary )*
/// unary        ::= "-" unary | call
/// call         ::= primary ( "(" arguments? ")" )* | keyword "(" arguments? ")"
/// primary      ::= number | ident | table_column
/// keyword      ::= keyword
/// table_column ::= ident "!" ident
/// arguments    ::= expr ("," expr) *

function error(ctx, expected, actual) {
  // Note: use custom error class
  const err = Error("Unexpected");
  err.ctx = ctx;
  err.expected = expected;
  err.actual = actual;
  throw err;
}

/*
 * =============
 * Global States
 * =============
 */

let lexer = null;

///// Functions

function next(type, ctx) {
  const t = lexer();
  if (t.type !== type) {
    error(ctx, [type], t);
  } else {
    return t;
  }
}
function peek() {
  return lexer.peek();
}
function peek2() {
  return lexer.peek(2);
}


function expr(ctx) {
  ctx.push({ context: 'expr' });
  
  const result = operation(ctx);

  ctx.pop();

  return result;
}

function operation(ctx) {
  const operands = [unary(ctx)];
  const operators = [];

  while (peek().type === 'operator') {
    operators.push( next('operator', ctx) );
    operands.push(unary(ctx));
  }

  if (operands.length > 1) {
    return {
      type: 'operation',
      operands,
      operators,
    };
  } else {
    return operands[0];
  }
}

function unary(ctx) {
  const token = peek();

  if (token.type === 'operator' && token.op === '-') {
    next('operator', ctx);
    return {
      type: 'unary_op',
      op: token.op,
      operand: unary(ctx),
    };
  } else {
    return call(ctx);
  }
}

function call(ctx) {
  const pri = primary(ctx);
  const token = peek();

  const isFunction = pri.type === 'keyword';

  if (isFunction || token.type === 'left_paren') {

    ctx.push({ context: 'func_call', callee: pri });

    next('left_paren', ctx);
    const args = $arguments(ctx)
    next('right_paren', ctx);

    let result = {
      type: 'call_expr',
      callee: pri,
      args,
    };

    ctx.pop();

    return result;
  } else {
    return pri;
  }
}
function primary(ctx) {
  const [nextT, nextTT] = peek2();

  let result;
  switch (true) {
    case nextT.type === "ident" && nextTT.type === "excl":
      result = tableCol(ctx);
      break;
    case nextT.type === 'keyword':
      result = next("keyword", ctx);
      break;
    case nextT.type === "ident":
      result = next("ident", ctx);
      break;
    case nextT.type === "number":
      result = next("number", ctx);
      break;
    default:
      error(ctx, ["keyword", "number", "ident"], nextT);
  }

  return result;
}
function tableCol(ctxs) {
  const ctx = { context: "table_col" };
  ctxs.push(ctx);

  const table = next("ident", ctxs);
  ctx.table = table.value;

  next("excl", ctxs);
  const column = next("ident", ctxs);

  ctxs.pop();

  return {
    type: "table_column",
    table: table.value,
    column: column.value
  };
}
function $arguments(ctx) {
  ctx.push({ context: 'arguments' });

  if (peek().type === 'right_paren') {
    ctx.pop();
    return [];
  }

  const args = [expr(ctx)];

  while (peek().type === 'comma') {
    next('comma', ctx);
    args.push(expr(ctx));
  }

  ctx.pop();

  return args;
}


export default function parse(_lexer) {
  lexer = _lexer;

  try {
    const result = expr([]);
    return { type: "ok", result };
  } catch (err) {
    const { ctx, expected, actual } = err;
    return { type: "err", error: { ctx, expected, actual } };
  } finally {
    lexer = null;
  }
}
