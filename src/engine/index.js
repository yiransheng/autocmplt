import lex, { keywords } from "./lexer";
import parse from "./parser";

const defaultSuggestions = {
  context: "none",
  items: []
};

export function trySuggest(state) {
  let { value: input, cursorAt } = state;
  if (!input || !input.startsWith("=")) {
  // In production only suggestions needs to be returned
    return {
      tokens: [],
      suggestions: defaultSuggestions,
      result: { type: "ok", result: null }
    };
  }

  const tokens = collectTokens(lex(input, cursorAt));
  const lexer = lex(input, cursorAt);

  let result = { type: "ok", result: null };
  // prevent inf loop
  for (let i = 0; i < 50; i++) {
    result = parse(lexer);
    if (i=== 0) {
      console.log('Result', result);
    }

    if (canAutocompleteResult(result)) {
      return {
        tokens,
        suggestions: getSuggestions(result),
        result
      };
    }

    if (lexer.peek().type === "eof" || lexer.peek().type === "eoft") {
      break;
    }
    lexer();
  }

  return {
    tokens,
    suggestions: defaultSuggestions,
    result
  };
}

function collectTokens(lexer) {
  const tokens = [];
  while (true) {
    const token = lexer();
    tokens.push(token);
    if (token.type === "eof" || token.type === "eoft") {
      return tokens;
    }
  }
}

function canAutocompleteResult(result) {
  if (result.type === "ok") {
    return false;
  }
  const { error } = result;
  const errorToken = error.actual;

  if (errorToken.type === "eof") {
    return true;
  }
  if (errorToken.type === "eoft") {
    return errorToken.token.type === "ident";
  }

  return false;
}

function getSuggestions(result) {
  const { ctx, actual } = result.error;

  const tokenValue = actual.type === "eof" ? "" : actual.token.value;
  const tokenRange = actual.range;

  for (let i = ctx.length - 1; i >= 0; i--) {
    const context = ctx[i];

    switch (context.context) {
      case "table_col":
        return { tokenRange, ...getColumnNames(context.table, tokenValue) };
      case "call": {
        const suggestions = getArgs(context.func, tokenValue);
        return {
          tokenRange,
          ...(suggestions.items.length ? suggestions : getFuncNames(tokenValue))
        };
      }
      case "expr":
        return { tokenRange, ...getFuncNames(tokenValue) };
      default:
    }
  }

  return defaultSuggestions;
}

function getFuncNames(word) {
  const items = Object.keys(keywords).filter(kw => {
    return kw.toLowerCase().startsWith(word.toLowerCase()) || !word;
  });

  return {
    context: 'func_name',
    items,
  };
}

const tables = {
  Table1: ["foo", "bar", "baz"],
  Table2: ["column1", "column2", "column3"]
};
function getColumnNames(tableName, word) {
  if (tables[tableName]) {
    return {
      context: "table_col",
      items: tables[tableName].filter(col => {
        return col.toLowerCase().startsWith(word.toLowerCase()) || !word;
      })
    };
  }

  return {
    context: "table_col",
    items: []
  };
}
const dfNames = ["Table1", "Table2", "My Dataframe", "Sorted Dataframe"];

function getArgs(funcName, word) {
  if (funcName === "DATAFRAME") {
    return {
      context: "arg",
      items: dfNames.filter(name => {
        return name.toLowerCase().startsWith(word.toLowerCase()) || !word;
      })
    };
  }

  return defaultSuggestions;
}
