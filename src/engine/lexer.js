import Lexer from "./Lexer";

export const keywords = {
  SUM: true,
  DIFF: true,
  MIN: true,
  MAX: true,
  DATAFRAME: true,
  SORT: true
};

let at = 0;

const lexer = Lexer()
  .addRule(/\s+/, function(whites) {
    // ignore whites
    at += whites.length;
  })
  .addRule(/\(/, function(lexeme) {
    at++;

    return {
      type: "left_paren",
      range: [at - 1, at]
    };
  })
  .addRule(/\)/, function(lexeme) {
    at++;

    return {
      type: "right_paren",
      range: [at - 1, at]
    };
  })
  .addRule(/!/, function(lexeme) {
    at++;

    return {
      type: "excl",
      range: [at - 1, at]
    };
  })
  .addRule(/\,/, function() {
    at++;

    return {
      type: "comma",
      range: [at - 1, at]
    };
  })
  .addRule(/[\+\-\*\/]/, function(lexeme) {
    at++;

    return {
      type: "operator",
      op: lexeme,
      range: [at - 1, at]
    };
  })
  .addRule(/[_a-zA-Z][_a-zA-Z0-9]*/, function(lexeme) {
    const len = lexeme.length;
    at += len;

    if (keywords[lexeme.toUpperCase()]) {
      return {
        type: "keyword",
        value: lexeme.toUpperCase(),
        range: [at - len, at]
      };
    } else {
      return {
        type: "ident",
        value: lexeme,
        range: [at - len, at]
      };
    }
  })
  .addRule(/\d+/, function(lexeme) {
    const len = lexeme.length;
    at += len;

    return {
      type: "number",
      value: parseInt(lexeme, 10),
      range: [at - len, at]
    };
  })
  .addRule(/./, function() {
    at++;
  });

function peekable(fnIter) {
  const cache = [];

  function next() {
    const item = fnIter();

    if (cache.length) {
      cache.push(item);
      return cache.shift();
    } else {
      return item;
    }
  }
  next.peek = function(count = 1) {
    count = +count;
    if (!count || count < 0) {
      count = 1;
    }
    const n = Math.max(0, count - cache.length);
    for (let i = 0; i < n; i++) {
      const item = fnIter();
      cache.push(item);
    }
    if (count > 1) {
      return cache.slice(0, count);
    } else {
      return cache[0];
    }
  };

  return next;
}

export default function lex(input, cursorAt) {
  at = 0;
  cursorAt = Math.min(cursorAt || 0, input.length);
  lexer.setInput(input);

  let eof = null;

  return peekable(function nextToken() {
    if (eof !== null) {
      return eof;
    }
    const token =
      lexer.lex() ||
      (eof = {
        type: "eof",
        range: [input.length, input.length]
      });
    const [start, end] = token.range;

    switch (token.type) {
      case "ident":
      case "keyword":
      case "number": {
        if (cursorAt > start && cursorAt <= end) {
          eof = {
            type: "eoft",
            token,
            range: token.range
          };
          return eof;
        } else {
          return token;
        }
      }
      default: {
        const underCursor = cursorAt <= end;
        if (underCursor) {
          eof = {
            type: "eof",
            range: [cursorAt, cursorAt]
          };
        }
        return token;
      }
    }
  });
}
