export function edit(payload) {
  return {
    type: "EDITED",
    payload
  };
}

export function autocomplete(range, replacement) {
  return {
    type: "AUTOCOMPLETE",
    payload: { range, replacement }
  };
}
