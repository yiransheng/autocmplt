import React from "react";
import { connect } from "react-redux";
import { getTokens } from "./selectors";

const HOC = connect(state => {
  return {
    tokens: getTokens(state),
    input: state.value
  };
});

function Token({ isActive, type, value, range, token, onHover }) {
  if (type === "eoft" && token) {
    value = token.value;
  }
  const className = isActive ? "token active" : "token";

  return (
    <div className={className} onMouseOver={onHover}>
      <div className="token-head">{type}</div>
      <div className="token-value">{value || "--"}</div>
      <div className="token-range">{range.join(" - ")}</div>
    </div>
  );
}

function InnerTokens({ tokens, active, onHover }) {
  if (!tokens.length) {
    tokens = [{
      type: 'none',
      value: '',
      range: [0, 0],
    
    }];
  }

  return (
    <div className="tokens">
      {tokens.map((t, i) => (
        <Token key={i} {...t} isActive={active === t} onHover={() => onHover(t, i)} />
      ))}
    </div>
  );
}

class Tokens extends React.Component {
  state = {
    active: null
  };

  componentWillReceiveProps() {
    this.setState({ active: null });
  }

  segmentInput() {
    const { input } = this.props;
    const { active } = this.state;
    if (active) {
      const [start, end] = active.range;
      const before = input.slice(0, start);
      const token = input.slice(start, end);
      const after = input.slice(end);
      return [
        { type: "raw", value: before },
        { type: "active", value: token },
        { type: "remain", value: after }
      ];
    } else {
      return [
        {
          type: "raw",
          value: input
        }
      ];
    }
  }
  handleHoverToken = (token) => {
    this.setState({ active: token });
  }

  render() {
    const { tokens } = this.props;
    return (
      <div>
      <h3>Lexer Output</h3>
      <div className="token-input">
        {this.segmentInput().map(({ type, value }) => {
          const className = type === "active" ? "active-token" : "raw-input";
          return (
            <span key={type} className={className}>
              {value}
            </span>
          );
        })}
      </div>
        <InnerTokens
          tokens={tokens}
          active={this.state.active}
          onHover={this.handleHoverToken}
        />
      </div>
    );
  }
}

export default HOC(Tokens);
