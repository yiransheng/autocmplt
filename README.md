## Autocompletion High Level Goals

**Input:**
a string and a cursor position (number) - ignore text range selection for now


**Output:**

```
type EditorState =
   | Nothing  -- nothing to do, string does not start with "=" for example
   | AutoComplete { ctx: Ctx, ident: Ident } -- autocompleting something, needs an Ctx type (completing funcName or argument or table column)
   | Info { .. }  

```

 

## Strategy

Two phases: 

1. Lexing
2. Try parsing (more on this later)



![](./screenshot.png)

## Lexing

#### Observations: 

1. White spaces are insignificant (except inside quoted strings)
2. we don't need to examine input after cursor position, for practical purposes, we can treat cursor as EOF token.

#### Lexing phase:

* Handle "=" outside lexing, if string does not start with "=", ignore subcequent steps
* Strip / ignore white spaces
* Parse string into a list of tokens, up to cursor position (using some regular gramma)
* Handle cursor EOF with special rules
* Lexer should recognize known keyword (excel function names - use keyword instead of ident token type)
* Lexing should never fail
* Each token should store a "range" property to help actual autocompletion at a later stage

#### EOF rules:

* **Notation:** use underscore to indicate cursor position

Example input:  `"=SUM(_, something else, might be junk"`
Expected Outupt: `[Keyword(SUM), LeftParen, EOF]`

Note: in this case, we treat cursor as `EOF`, and send the three tokens to parsing phase

Example 2: `"=DI_F(junk after this point..."`
Expected Output:  `[EOFT(Ident("DIF"))]`

In this case, we introduce an `EOF` type that holds some data (a partial identifier) - `EOFT`. To the parser, this is the same as `EOF`.

However, by storing the partial token inside it, we can use it for autocompletion.

To accomplish this, lexer needs to be smart enough to detect if cursor is at an empty position (like case 1), or in the middle of a word/identifier, and extract the whole word (not just the parts left of the cursor) to put inside `EOFT`.





## Try Parsing Phase

Observe:

* If the tokens produced from lexing parses to a valid function call expression - there's no need to autocomplete, as whole thing is already valid and good to run

Thus the focus of our attention is how to deal with parse error and recover autocompletion context from the error object.

Define Error:

* For now, parser either succeeds or report an "Unexpected Token" error type (no other error type needed)

Steps:

1. First parse the tokens, could result in three outputs:
    a) Ok - nothing do to
    b) Unexpected Token EOF/EOFT - parser recognizes input tokens up until cursor
    c) Unexpected Token something else - parser fails before reaching cursor

For now, let's focus on senario b) - the perfect case for deducing autocompletion context.

The code block blow implements a very basic recursive decent parser - but with sufficient error handling. Reported error object holds:

```
type UnexpectedTokenError = {

  ctx: /* Some context info, this can be tweaked and defined better */,

  expected: Array<TokenType>,

  actual: Token,

};
```



For example, if parsing produces UnexpectedTokenError object error, we can use it like so:

```
error.actual      // a EOFT token type, holds current word under cursor

error.expected // a list of token types (ident, keyword, right_paren etc.), for instance ["ident"]

error.ctx            // ctx.stage -> "parse_table_col", we failed while parsing table column
```

Now we know we need a "ident", and the context is parsing table column some thing like, "Table!" and we can recover table name from ctx as well. Using this info, we can look up the schema and put up a suggestion list.



#### What to do for scenario c)?

What if parser fails before reaching EOF/Cursor, for instance for this input:

Example: `"=SUM))Bad Syntax)DATAFRA_`

We should still attempt to autocomplete the work "DATAFRA" to "DATAFRAME"

A straight-forward strategy is if parser fails at token i, and it's not EOF/EOFT, we simply start parsing all over again from token i+1. Repeat this enough times, we will come back to scenario b) or a), which we know how to handle.

In the above case we will get to parsing "DATAFRA_" part (all preceding strings/tokens do not parse correctly).

I am certain this will NOT cover all bases, and we may fail to handle some legit input strings. In IDEs and compilers, usually there's something called synchronization on parse error - typically restarting parsing on the next line or something. I am not familiar with these techniques.



## Run the Code here

```
yarn start
```

Caveats:

* Lexer in this repo does not deal with quoted and half quoted strings
* Very few function names are hardcoded in (`SUM`, `MAX`, `DIFF` and a few others)
* It used work with `=DATAFRAME(` - suggestion a list of hardcoded table names, but I broke it at some point, now it cannot deal with autocomplete function argument
* Still works with function name autocompletion and table column autocompletion (use `Table1` as table name, also hardcoded)



Grammar for formula used here:

```
/// Grammar
///

/// expr         ::= operation
/// operation    ::= unary ( "+" | "-" | "*" | "/" unary )*
/// unary        ::= "-" unary | call
/// call         ::= primary ( "(" arguments? ")" )* | keyword "(" arguments? ")"
/// primary      ::= number | ident | table_column
/// table_column ::= ident "!" ident
/// arguments    ::= expr ("," expr) *
-----
/// Terminals: "+" "-" "*" "/" "(" ")" "!" "," number ident keyword
```

Designing a easy to parse grammar for this is surprisingly hard, and different grammar can drastically impact parsing context and autocompletion semantics. This is the best I came up with, but careful thoughts need to be put into it if we decided to go this route. 

This grammar is LL(1), thus can be parsed by recursive decent. A few things are still missing - for instance use parenthesis not in function calls but in arithmetic grouping, eg. "=A * (B + C)". 